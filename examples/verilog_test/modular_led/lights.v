`include "../icoboard_components/PMOD1_BUTTON.v"
`include "../icoboard_components/PMOD2_SWITCH.v"
`include "../icoboard_components/PMOD4_LED.v"
`include "../components/selective_invert.v"
module top(in_bits, button_bits, out_bits);
    output [0:7] out_bits;
    input  [0:7] in_bits;
    wire   [0:7] bits_con_in, bits_con_out;
    input  [0:3] button_bits;
    wire   [0:3] button_wire;
    PMOD1_BUTTON button(button_bits, button_wire);
    PMOD2_SWITCH switch(in_bits, bits_con_in);
    selective_invert8 inv(bits_con_out, button_wire[3], bits_con_in);
    PMOD4_LED led(.in(bits_con_out), .PMOD4_LED_out(out_bits));
endmodule