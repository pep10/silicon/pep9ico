`include "../icoboard_components/PMOD1_BUTTON.v"
`include "../icoboard_components/PMOD2_SWITCH.v"
`include "../icoboard_components/PMOD3_LED.v"
`include "../icoboard_components/PMOD4_LED.v"
`include "../components/selective_invert.v"
`include "../components/adder.v"
`include "../components/dff.v"
`include "../components/tff.v"
`include "../components/buslogic.v"
`include "../components/mux.v"
// p1_in is a button board plugged into the bottom of P1.
// p1_in[3] is used as a clock.
// p1_in[2] is a function select.
// p2_in[0:3] is the bottom switch, and is bound to the A bus.
// p2_in[4:7] is the top switch, and is bound to the B bus.
// P3, P4 are LED boards.
// rled_out is the red LED on the icoboard.
module top(input [0:3] p1_in, input [0:7]p2_in,
           output [0:7] p3_out, output[0:7] p4_out,
           output rled_out);
    // Inputs from PMODS
    wire   [0:7] p2_wire;
    wire   [0:3] p1_wire;
    PMOD1_BUTTON button(p1_in, p1_wire);
    PMOD2_SWITCH switch(p2_in, p2_wire);

    // Internal logic
    wire   [0:7] p3_wire;
    wire   [0:7] a_ext, b_ext;
    // Extend the 4 bit inputs to use 8 bit logic circuits.
    // MUST clear high order bits, or circuit synthesizes to junk.
    assign a_ext[0:3] = 4'h0;
    // Assign A bus from top switch.
    assign a_ext[4:7] = p2_in[0:3];
    assign b_ext[0:3] = 4'h0;
    // Assign B bus from bottom switch.
    assign b_ext[4:7] = p2_in[4:7];

    // Wires to connect functional units to DFF.
    wire   [0:7] temp_wire1, temp_wire2;
    // Wires to connect DFF to Mux.
    wire   [0:7] dff_1, dff_2;

    // Functional unit that performs addition.
    adder4v add(temp_wire1[4:7], temp_wire1[3], temp_wire1[2], p2_in[0:3], p2_in[4:7], 1'b0, 1'b0);
    // Must clear high order bits, or circuit will synthesize to junk.
    assign temp_wire1[0:2] = 3'b000;

    // Functional unit that performs logical and.
    andor8 andor(temp_wire2, a_ext, b_ext, 1'b0, 1'b0 );

    // Store the results of ADD/AND
    dff8 dff1(p1_in[3], temp_wire1, dff_1);
    dff8 dff2(p1_in[3], temp_wire2, dff_2);
    // Multiplex DFF's to LED board in PMOD3.
    mux2xn #(8) muxer(p3_wire, rled_out, dff_1, dff_2);

    // Togle latch that switches between outputting ADD/AND output.
    tff1 tff(p1_in[2], 1'b1, rled_out);

    // Outputs to PMODS
    PMOD3_LED led3(.in(p3_wire), .PMOD3_LED_out(p3_out));
    PMOD4_LED led4(.in(p2_wire), .PMOD4_LED_out(p4_out));
endmodule