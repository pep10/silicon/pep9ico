`include "./add.v"
// Test that the add'ing module works correctly
module testbench();
    // Declare wire / registers as required by top.
    reg [0:3] p1_in; 
    reg [0:7] p2_in;
    reg [0:7] a, b;
    wire [0:7] p3_out, p4_out;
    wire rled_out;

    // Output from andor gate
    wire [0:7] and_out;

    // Create main module.
    top topmod(p1_in, p2_in, p3_out, p4_out, rled_out);
    // Test that the new and/or module works
    andor8 andy(and_out, a, b, 1'b0, 1'b0);
    initial begin
        // Assign distinct values to A, B.
        a <= 8'hDE;
        b <= 8'hAD;
        // Disable button inputs.
        p1_in <= 4'h0;
        //
        p2_in <= 8'b10010011;
        $display("Time    RL P1   P3   P4  which");
        // Monitor all non-const inputs/outputs.
        $monitor ("%g\t %b  %h   %h   %h  %h" , $time, rled_out, p1_in, p3_out, p4_out, and_out);
        // End simulation after 6 steps.
        #30;
        $finish;
    end
    always begin
        // Toggle clock (p[3]) on and off every 5ms.
        // Also toggle the ADDER/ANDER (p[2]) every full cycle.
        #5 p1_in[3]=!p1_in[3];
        #5 p1_in[3]=!p1_in[3];
        p1_in[2]=!p1_in[2];
    end
endmodule