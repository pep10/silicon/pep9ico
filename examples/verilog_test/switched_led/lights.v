module top(in_bits, out_bits);
    output [0:7] out_bits;
    input   [0:7] in_bits;
    wire [0:7] bits_con;
    // Never directly connect inputs to outputs - buffer first.
    buf bufi_0(in_bits[0], bits_con[0]);
    buf bufi_1(in_bits[1], bits_con[1]);
    buf bufi_2(in_bits[2], bits_con[2]);
    buf bufi_3(in_bits[3], bits_con[3]);
    buf bufi_4(in_bits[4], bits_con[4]);
    buf bufi_5(in_bits[5], bits_con[5]);
    buf bufi_6(in_bits[6], bits_con[6]);
    buf bufi_7(in_bits[7], bits_con[7]);

    buf bufo_0(out_bits[0], bits_con[0]);
    buf bufo_1(out_bits[1], bits_con[1]);
    buf bufo_2(out_bits[2], bits_con[2]);
    buf bufo_3(out_bits[3], bits_con[3]);
    buf bufo_4(out_bits[4], bits_con[4]);
    buf bufo_5(out_bits[5], bits_con[5]);
    buf bufo_6(out_bits[6], bits_con[6]);
    buf bufo_7(out_bits[7], bits_con[7]);
endmodule