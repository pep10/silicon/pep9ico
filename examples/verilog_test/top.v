`include "dff.v"
`include "components/dff.v"
module top();
    reg d=0, cl=0;
    wire q, q_bar;
    dff mydff(d, cl, q, q_bar);

    reg o_d=1;
    wire o_q;
    dff1 other_dff(cl, o_d, o_q);

    reg [0:7] long_d = 8'hCA;
    wire [0:7] long_q;
    dff8 long_dff(cl, long_d, long_q);

    initial begin
        $display ("time\t clk  d      q");
        //$monitor ("%g\t %b   %b     %b " , $time, cl, d, q);
        //$monitor ("%g\t %b   %b     %b " , $time, cl, o_d, o_q);
        $monitor ("%g\t %b   %h     %h " , $time, cl, long_d, long_q);
        #20;
        $finish;
    end

    // Clock generator
    always begin
        #5  cl = ~cl; // Toggle clock every 5 ticks
        #5  d=~d;
        cl = ~cl; // Toggle clock every 5 ticks
        o_d = ~o_d;
        long_d = ~ long_d;
    end
endmodule