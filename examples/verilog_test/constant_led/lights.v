module top(bits);
    output  [0:15] bits = 16'hDEAD;
    wire [0:15] bits_wires;
    assign bits_wires = bits;
endmodule