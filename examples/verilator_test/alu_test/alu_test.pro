TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

TARGET = alu_test

exists(/usr/local/share/verilator/include) {
    INCLUDEPATH += /usr/local/share/verilator/include
    VPATH += /usr/local/share/verilator/include
}
else:exists(/usr/share/verilator/include) {
    INCLUDEPATH += /usr/share/verilator/include
    VPATH += /usr/share/verilator/include
}

HEADERS += \
        alufunc.h \
        cpu.h \
        defs.h



SOURCES += \
        tests/main.cpp \
        verilated.cpp \
        alufunc.c \
        cpu.c \

DISTFILES += \
        alu_test.v \
        alu_test.pcf \
        testbench.v \
        Makefile


INCLUDEPATH += $$OUT_PWD/obj_dir/

VPATH += $$PWD/../../../../pep9milli/simmain
INCLUDEPATH += $$PWD/../../../../pep9milli/simmain

# Generate verilator source code from verilog implementation.
verilator.target = $$OUT_PWD/obj_dir/Valu_driver.h
verilator.commands += $$VERILATOR_PATH --cc $$PWD/alu_test.v --top-module alu_driver  -Wno-LITENDIAN

unix{
    # Build verilator source code into a library
    make_verilate.target = $$OUT_PWD/obj_dir/libValu_driver__ALL.a
    make_verilate.commands += cd $$OUT_PWD/obj_dir/ && make -f Valu_driver.mk && mv Valu_driver__ALL.a libValu_driver__ALL.a
    make_verilate.depends = verilator
    # Add the ability to clean
    verilator_clean.commands = rm -rf $$OUT_PWD/obj_dir/
    clean.depends += verilator_clean
}
# Not yet implemented in Windows
win{
}

# Add verilog targets.
QMAKE_EXTRA_TARGETS += verilator make_verilate verilator_clean clean
# Force QMake to wait on Verilator output before building
unix{
    PRE_TARGETDEPS += $$OUT_PWD/obj_dir/Valu_driver.h $$OUT_PWD/obj_dir/libValu_driver__ALL.a
}
win{
}

# Link against verilator library.
unix|win32: LIBS += -L$$OUT_PWD/obj_dir/ -lValu_driver__ALL

# Make project aware of outputs from Verilator
INCLUDEPATH += $$OUT_PWD/obj_dir/
DEPENDPATH += $$OUT_PWD/obj_dir/
