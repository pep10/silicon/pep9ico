`include "alu_test.v"
// Test that the add'ing module works correctly
module testbench();
    // Declare wire / registers as required by top.
    reg [0:3] p1_in;
    reg [0:7] p2_in;
    wire [0:7] p3_out, p4_out;
    wire redled_out;
    wire rgreenled_out;
    wire lgreenled_out;
    // Create item-under-test;
    logic [0:7] temp;
    alu_driver pep9alu(p1_in, p2_in, p3_out, p4_out, redled_out, rgreenled_out, lgreenled_out);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars(0, testbench);
    end
    
    initial begin
        p1_in = 4'h0; p2_in = 8'h01;
        $display("Time      State   P1  P2   Out  Flex");
        // Monitor all non-const inputs/outputs.
        $monitor ("%g\t   %b%b     %b   %h   %h   %h" , $time, rgreenled_out, lgreenled_out , p1_in[2], p2_in, p3_out, pep9alu.alu.ALUFuncDecoder.y);

        // Assign a value to A
        p2_in = 8'hde;
        pep9alu.tff_lo.Q=0; pep9alu.tff_hi.Q=0;
        #1;p1_in[2] = 1; #1;p1_in[2] = 0;

        // Assign a value to B
        pep9alu.tff_lo.Q=1; pep9alu.tff_hi.Q=0;
        p2_in = 8'had;
        #1;p1_in[2] = 1; #1;p1_in[2] = 0;

        // Assign a value to ALUFunc
        p2_in = 8'h01;
        pep9alu.tff_lo.Q=0; pep9alu.tff_hi.Q=1;
        #1;p1_in[2] = 1; #1;p1_in[2] = 0;

        // Assign value to output
        p2_in = 8'h00;
        pep9alu.tff_lo.Q=1; pep9alu.tff_hi.Q=1;
        #1;p1_in[2] = 1; #1;p1_in[2] = 0;

        // Display A
        p2_in = 8'h01;
        pep9alu.tff_lo.Q=1; pep9alu.tff_hi.Q=1;
        #1;p1_in[2] = 1; #1;p1_in[2] = 0;

        // Display B
        p2_in = 8'h02;
        pep9alu.tff_lo.Q=1; pep9alu.tff_hi.Q=1;
        #1;p1_in[2] = 1; #1;p1_in[2] = 0;
        //$monitor ("%g\t %b  %h   %h   %h" , $time, state , p1_in, p3_out, p4_out);
        // End simulation after 6 steps.
        #50;
        $finish;
    end
    always begin
        // Toggle clock (p[3]) on and off every 5ms.
        // Also toggle the ADDER/ANDER (p[2]) every full cycle.
        #15 p1_in[3]=!p1_in[3];
    end
    always begin
        #100;
    end
endmodule