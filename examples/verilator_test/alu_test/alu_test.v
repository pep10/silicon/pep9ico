`include "../components/adder.v"
`include "../components/buslogic.v"
`include "../components/counter.v"
`include "../components/demux.v"
`include "../components/dff.v"
`include "../components/enable.v"
`include "../components/mux.v"
`include "../components/selective_invert.v"
`include "../components/shifter.v"
`include "../components/tff.v"
`include "../icoboard_components/PMOD1_BUTTON.v"
`include "../icoboard_components/PMOD2_SWITCH.v"
`include "../icoboard_components/PMOD3_LED.v"
`include "../icoboard_components/PMOD4_LED.v"
`include "../subsystems/alu.v"
// p1_in is a button board plugged into the bottom of P1.
// p1_in[3] is used as cycle clock
// p1_in[2] is a data clock
// p2_in[0:3] is the bottom switch, and is bound to the A bus.
// p2_in[4:7] is the top switch, and is bound to the B bus.
// P3, P4 are LED boards.
// rled_out is the red LED on the icoboard.
module alu_driver(input [0:3] p1_in, input [0:7]p2_in,
           output [0:7] p3_out, output[0:7] p4_out, output [0:8] GPIO,
           output redled_out, output rgreenled_out, output lgreenled_out);
    // Inputs from PMODS
    wire   [0:7] p2_wire, data_in;
    wire   [0:3] p1_wire;
    PMOD1_BUTTON button(p1_in, p1_wire);
    PMOD2_SWITCH switch(p2_in, p2_wire);
    wire clock, data_clock;
    assign clock = p1_wire[3];
    assign data_clock = p1_wire[2];
    assign data_in = p2_wire;

    // Create two bit state machine.
    reg [0:1] state;
    reg reset;
    counter #(2,3) state_counter(clock, 1'b0, state);

    // Connect green LED's to state FSM state.
    buf led1(lgreenled_out, state[1]);
    buf led2(rgreenled_out, state[0]);

    // Create DFF's for functions, bus values
    wire AClock, BClock, ALUFuncClock, ModClock;
    wire [0:7] AValue, BValue, ALUFuncValue, ModValue;
    dff8 A_latch(AClock, data_in, AValue);
    dff8 B_latch(BClock, data_in, BValue);
    dff8 ALU_latch(ALUFuncClock, data_in, ALUFuncValue);
    dff8 MOD_latch(ModClock, data_in, ModValue);

    // Drive clocks
    and AClock_gen(AClock, !state[0], !state[1], data_clock);
    and BClock_gen(BClock, !state[0], state[1], data_clock);
    and ALUFuncClock_gen(ALUFuncClock, state[0], !state[1], data_clock);
    and ModClock_gen(ModClock, state[0], state[1], data_clock);

    // The holy ALU lives.
    wire [0:7] ALU_out;
    wire NW, ZW, VW, CW;
    PEP9_ALU alu(ALU_out, NW, ZW, VW, CW, 
        AValue, BValue, ALUFuncValue[4:7], ALUFuncValue[3]);
    // Outputs to PMODS
    wire [0:7] p3_wire;
    mux4x8 outmux(p3_wire, ModValue[6:7], ALU_out, AValue, BValue, {NW, ZW, VW, CW, ALUFuncValue[4:7]});
    assign GPIO[0:7] = p3_wire;
    assign GPIO[8] = ALUFuncValue[3];
    // Must do something with status bits, or they are dropped by simulation/emulation tools.
    //dff8 stat_latch(p1_wire[1], {4'b000,NW, ZW, VW, CW}, p4_out);
    and(redled_out, ALUFuncValue[3]);
    //assign p3_wire = AValue;
    PMOD3_LED led3(.in(p3_wire), .PMOD3_LED_out(p3_out));
    PMOD4_LED led4(.in(p2_wire), .PMOD4_LED_out(p4_out));
endmodule
