import pigpio
import time
pi = pigpio.pi()
now = int(round(time.time() * 1000)) 

def cbr(GPIO, level, tick):
	global now
	alt_time = int(round(time.time() * 1000))
	if(alt_time - now) >  10:
		now = alt_time
		print(read())
def read(GPIO=None, level=None, tick=None):
	arr = 9 * [None]
	arr[0] = pi.read(16) 
	arr[1] = pi.read(17)
	arr[2] = pi.read(18)
	arr[3] = pi.read(19)
	arr[4] = pi.read(20)
	arr[5] = pi.read(21)
	arr[6] = pi.read(22)
	arr[7] = pi.read(23)
	arr[8] = pi.read(24)
	return arr
which = [16, 17, 18, 19, 20, 21, 22, 23]
for pin in which:
	pi.callback(pin, pigpio.EITHER_EDGE, cbr)
