`include "../components/adder.v"
`include "../components/buslogic.v"
`include "../components/counter.v"
`include "../components/demux.v"
`include "../components/dff.v"
`include "../components/enable.v"
`include "../components/bram512x8.v"
`include "../components/mux.v"
`include "../components/selective_invert.v"
`include "../components/shifter.v"
`include "../components/tff.v"
`include "../icoboard_components/PMOD1_BUTTON.v"
`include "../icoboard_components/PMOD2_SWITCH.v"
`include "../icoboard_components/PMOD3_LED.v"
`include "../icoboard_components/PMOD4_LED.v"
`include "../subsystems/alu.v"
`include "../subsystems/clock_gen.v"
`include "../subsystems/regbank.v"
// p1_in is a button board plugged into the bottom of P1.
// p1_in[3] is used as cycle clock
// p1_in[2] is a data clock
// p2_in[0:3] is the bottom switch, and is bound to the A bus.
// p2_in[4:7] is the top switch, and is bound to the B bus.
// P3, P4 are LED boards.
// rled_out is the red LED on the icoboard.
module register_bank_driver(input [0:3] p1_in, input [0:7]p2_in, input clock,
           output [0:7] p3_out, output[0:7] p4_out,
           output redled_out, output rgreenled_out, output lgreenled_out);
    // Inputs from PMODS
    wire   [0:7] p2_wire, p4_wire;
    wire   [0:3] p1_wire;
    PMOD1_BUTTON button(p1_in, p1_wire);
    PMOD2_SWITCH switch(p2_in, p2_wire);
	wire global_clock, global_reset, clock_enable, overflow;
	assign global_clock = p1_wire[3];
	assign global_reset = p1_wire[2];
	assign clock_enable = p1_wire[1];


    localparam clock_count = 9;
	wire [0:clock_count-1] enable_outputs;
	wire [0:7] AData, BData;
	wire [0:$clog2(clock_count)] count;
	wire [0:1] fsm_state;
	wire seq_rst, seq_en, seq_o_en;

	ClockSequenceGenerator #(clock_count) machine
						(.global_clock(global_clock),
							.reset(global_reset),
							.enable_step(clock_enable),
							.clock_outputs(enable_outputs),
							.fsm_state(fsm_state),
							.count(count));
	/*mem512x8 registers(.clock(global_clock),
					   .write_enable(1'b0),
					   .read_enable(fsm_state==2'b01),
					   .addr({4'b0000,count}),
					   .wdata(8'h00),
					   .rdata(data));*/
	RegisterBank registers(.ASelect(count), .BSelect(count),
						   .CSelect(5'h0), .CData(8'h00),
						   .AEnable(enable_outputs[0]|enable_outputs[2]|enable_outputs[4]|enable_outputs[6]),
						   .BEnable(enable_outputs[1]|enable_outputs[3]|enable_outputs[5]|enable_outputs[7]),
						   .CEnable(1'b0),
						   .ACkEnable(enable_outputs[1]|enable_outputs[3]|enable_outputs[5]|enable_outputs[7]),
						   .BCkEnable(enable_outputs[2]|enable_outputs[4]|enable_outputs[6]|enable_outputs[8]),
						   .WCkEnable(1'b0),
						   .clock(global_clock), .reset(global_reset),
						   .AData(AData), .BData(BData)
						   );
    // Connect green LED's to state FSM state.
	buf led1(lgreenled_out, fsm_state[0]);
	buf led2(rgreenled_out, fsm_state[1]);
	buf ledr(redled_out, clock_enable);
    // Must do something with status bits, or they are dropped by simulation/emulation tools.
    //dff8 stat_latch(p1_wire[1], {4'b000,NW, ZW, VW, CW}, p4_out);
    //assign p3_wire = AValue;
	PMOD3_LED led3(.in(BData),  .PMOD3_LED_out(p3_out));
	// Output
	PMOD4_LED led4(.in(AData), .PMOD4_LED_out(p4_out));
endmodule
