`include "register_bank_test.v"
// Test that the add'ing module works correctly
module testbench();
    // Declare wire / registers as required by top.
    reg [0:3] p1_in;
    reg [0:7] p2_in;
    wire [0:7] p3_out, p4_out;
    wire redled_out;
    wire rgreenled_out;
    wire lgreenled_out;
    reg global_clock, global_reset, clock_enable;
    // Create item-under-test;
    localparam clock_number =7;
	wire [0:clock_number-1] clocks;
	wire clock_out_enable, overflow;

    /*ClockSequenceGenerator #(clock_number) machine(.global_clock(global_clock),
                                   .reset(global_reset),
                                   .enable_step(clock_enable),
								   .clock_outputs(clocks));*/
    register_bank_driver uut(p1_in, p2_in, 1'b0, p3_out, p4_out, redled_out, rgreenled_out, lgreenled_out);
    initial begin
        p1_in = 0; p2_in = 0;
        //count = 0;
        {clock_enable, global_reset, global_clock} = 3'h0;
        global_reset = 1;
        global_clock = 1;
        #1 
        global_clock = 0;
        global_reset = 0;
        //$monitor ("%g\t   %b\t%b\t%b%b\t%g" , $time, p1_in,driver.driver.state_current, driver.outs,clock_out_enable, count);
		/*$monitor ("%g\t   %b\t%b\t%b%b%b\t%b\t%b" , $time, p1_in,machine.state_machine.state_current,
		         machine.sequencer_output_enable, machine.sequencer.reset,
				 machine.sequencer_overflow, machine.count, clocks);*/
        $monitor("%g\t %b %b\t%b\t %b %b", $time, uut.count, uut.enable_outputs, uut.registers.reg_out,uut.AData, uut.BData);
        #200;
        $finish();
    end

    always @(*) begin
        //p1_in[0] <= overflow;
        p1_in[1] <= clock_enable;
        p1_in[2] <= global_reset;
        p1_in[3] <= global_clock;
    end

    // Toggle the clock on and off
    always begin
        #5;
        global_clock = !global_clock;
    end

    always begin
        clock_enable = 1;
        #10;
        clock_enable = 0;
        #40;
        //overflow = 1;
    end

endmodule
