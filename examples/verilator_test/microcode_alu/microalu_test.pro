TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

TARGET = alu_test

exists(/usr/local/share/verilator/include) {
    INCLUDEPATH += /usr/local/share/verilator/include
    VPATH += /usr/local/share/verilator/include
}
else:exists(/usr/share/verilator/include) {
    INCLUDEPATH += /usr/share/verilator/include
    VPATH += /usr/share/verilator/include
}

HEADERS += \
        $$PWD/obj_dir/*.h \
        alufunc.h \
        cpu.h \
        defs.h



SOURCES += \
        tests/main.cpp \
        $$PWD/obj_dir/*.cpp \
        verilated.cpp \
        alufunc.c \
        cpu.c

INCLUDEPATH += $$PWD/obj_dir/

VPATH += $$PWD/../../../../pep9milli/simmain
INCLUDEPATH += $$PWD/../../../../pep9milli/simmain
