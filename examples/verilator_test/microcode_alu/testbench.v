`include "microalu_test.v"
// Test that the add'ing module works correctly
module testbench();
    // Declare wire / registers as required by top.
    reg [0:3] p1_in;
    reg [0:7] p2_in;
    wire [0:7] p3_out, p4_out;
    wire redled_out;
    wire rgreenled_out;
    wire lgreenled_out;
    // Create item-under-test;
    logic [0:7] temp;
    microalu_driver pep9alu(p1_in, p2_in, p3_out, p4_out, redled_out, rgreenled_out, lgreenled_out);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars(0, testbench);
    end
    
    initial begin
        p1_in = 4'h0; //p2_in = 8'h01;
        //#1 p1_in[3]=!p1_in[3];
        //#1 p1_in[3]=!p1_in[3];
        $display("Time       Counter Output");
        // Monitor all non-const inputs/outputs.
        $monitor ("%g\t   %b\t   %b" , $time, pep9alu.state, pep9alu.p3_wire);
        pep9alu.reset = 1'b0;
        //#1;
        #105;
        $finish;
    end
    always begin
        // Toggle clock (p[3]) on and off every 5ms.
        // Also toggle the ADDER/ANDER (p[2]) every full cycle.
        //#5 p1_in[2]=!p1_in[2];
        #1;
    end
    always begin
        // Toggle clock (p[3]) on and off every 5ms.
        // Also toggle the ADDER/ANDER (p[2]) every full cycle.
        #15 p1_in[3]=!p1_in[3];
    end
endmodule