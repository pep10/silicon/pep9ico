`include "../components/adder.v"
`include "../components/buslogic.v"
`include "../components/counter.v"
`include "../components/demux.v"
`include "../components/dff.v"
`include "../components/enable.v"
`include "../components/mux.v"
`include "../components/selective_invert.v"
`include "../components/shifter.v"
`include "../components/tff.v"
`include "../icoboard_components/PMOD1_BUTTON.v"
`include "../icoboard_components/PMOD2_SWITCH.v"
`include "../icoboard_components/PMOD3_LED.v"
`include "../icoboard_components/PMOD4_LED.v"
`include "../subsystems/alu.v"
`include "microcode.v"
// p1_in is a button board plugged into the bottom of P1.
// p1_in[3] is used as cycle clock
// p1_in[2] is a data clock
// p2_in[0:3] is the bottom switch, and is bound to the A bus.
// p2_in[4:7] is the top switch, and is bound to the B bus.
// P3, P4 are LED boards.
// rled_out is the red LED on the icoboard.
module microalu_driver(input [0:3] p1_in, input [0:7]p2_in,
           output [0:7] p3_out, output[0:7] p4_out,
           output redled_out, output rgreenled_out, output lgreenled_out);
    // Inputs from PMODS
    wire   [0:7] p2_wire, data_in;
    wire   [0:3] p1_wire;
    PMOD1_BUTTON button(p1_in, p1_wire);
    PMOD2_SWITCH switch(p2_in, p2_wire);
    wire clock;
    assign clock = p1_wire[3];

    // Create two bit state machine.
    wire [0:3] state;
    reg reset;
    assign reset = 0;
    counter #(4,15) state_counter(clock, reset, state);
    // Connect green LED's to state FSM state.
    buf led1(lgreenled_out, state[1]);
    buf led2(rgreenled_out, state[0]);
    wire [0:7] a_mc,b_mc;
    wire [0:3] alu_func_mc;
    wire [0:0] cin_mc;
    wire [0:1] display_mc;
    wire [0:22] all;
    basic_microcode_store store(a_mc, b_mc, alu_func_mc, cin_mc, display_mc, all, 1'b0, clock, state, 23'h000000);

    // The holy ALU lives.
    wire [0:7] ALU_out;
    wire NW, ZW, VW, CW;
    PEP9_ALU alu(ALU_out, NW, ZW, VW, CW, 
        a_mc, b_mc, alu_func_mc, cin_mc);
    // Outputs to PMODS
    wire [0:7] p3_wire;
    mux4x8 outmux(p3_wire, display_mc, ALU_out, a_mc, b_mc, {NW, ZW, VW, CW, alu_func_mc[4:7]});
    // Must do something with status bits, or they are dropped by simulation/emulation tools.
    //dff8 stat_latch(p1_wire[1], {4'b000,NW, ZW, VW, CW}, p4_out);
    and(redled_out, cin_mc);
    //assign p3_wire = AValue;
    PMOD3_LED led3(.in(p3_wire), .PMOD3_LED_out(p3_out));
    PMOD4_LED led4(.in({4'h00, state}), .PMOD4_LED_out(p4_out));
endmodule
