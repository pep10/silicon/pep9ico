#include <stdlib.h>
#include "Vmicroalu_driver.h"
#include "verilated.h"

extern "C" {
#include "alufunc.h"
#include "defs.h"
#include "cpu.h"
}

#include <vector>
#include <iostream>

void clock_state(Vmicroalu_driver *tb)
{

}

void test_one(Vmicroalu_driver *tb, uint8_t a, uint8_t b, uint8_t alu_mode, uint64_t& fails)
{


    // Skip over data output state.
    //assert(tb->microalu_driver__DOT__a_mc == a);
    //clock_state(tb);
    ALUByteResult result;
    switch(alu_mode & 0xf) {
    case 0:
        result = byte_ident(a); break;
    case 1:
        result = byte_add_nocarry(a, b); break;
    case 2:
        result = byte_add_carry(a, b, (alu_mode & 0x10)? 1 : 0); break;
    case 3:
        result = byte_sub_nocarry(a, b); break;
    case 4:
        result = byte_sub_carry(a, b, (alu_mode & 0x10)? 1 : 0); break;
    case 5:
        result = byte_and(a,b); break;
    case 6:
        result = byte_nand(a,b); break;
    case 7:
        result = byte_or(a,b); break;
    case 8:
        result = byte_nor(a,b); break;
    case 9:
        result = byte_xor(a,b); break;
    case 10:
        result = byte_not(a); break;
    case 11:
        result = byte_asl(a); break;
    case 12:
        result = byte_rol(a, (alu_mode & 0x10)? 1 : 0); break;
    case 13:
        result = byte_asr(a); break;
    case 14:
        result = byte_ror(a, (alu_mode & 0x10)? 1 : 0); break;
    case 15:
        result = byte_flags(a); break;
    }

    if(result.result != tb->microalu_driver__DOT__ALU_out) {
        printf("\tFail Result\ta:%x\tb:%x\tActual:%x\tExpected:%x\n",
               tb->microalu_driver__DOT__a_mc,
               tb->microalu_driver__DOT__b_mc, tb->microalu_driver__DOT__ALU_out, result.result);
        fails += 1;
    }
    // Compare status bits
    else if(!((result.NZVC[N] == ((tb->microalu_driver__DOT__NW) ? true : false)) &&
         (result.NZVC[Z] == ((tb->microalu_driver__DOT__ZW) ? true : false)) &&
         (result.NZVC[V] == ((tb->microalu_driver__DOT__VW) ? true : false)) &&
         (result.NZVC[C] == ((tb->microalu_driver__DOT__CW) ? true : false)))) {
        printf("\tFail Status\ta:%x \tb:%x\tval:%x\t\tCalc NZVC:%i%i%i%i\tReal NZVC:%i%i%i%i\n",
               tb->microalu_driver__DOT__a_mc,
               tb->microalu_driver__DOT__b_mc,
               tb->p3_out,
               (tb->microalu_driver__DOT__NW)?1:0,
               (tb->microalu_driver__DOT__ZW)?1:0,
               (tb->microalu_driver__DOT__VW)?1:0,
               (tb->microalu_driver__DOT__CW)?1:0,
               result.NZVC[N],result.NZVC[Z],result.NZVC[V],result.NZVC[C]);
        fails += 1;
    }
    else {
        printf("\tPassed test!\tOutput:\t");
        std::cout << std::bitset<8>(result.result) << std::endl;
    }
    tb->p1_in =0;tb->eval();
}

int main(int argc, char **argv) {
	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	Vmicroalu_driver *tb = new Vmicroalu_driver;
    //int tickc=0;
    std::vector<int> modes = {0x0, 0x1, 0x02, 0x12, 0x3, 0x04, 0x14,
                              0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0x0C,
                              0x1C, 0xD, 0x0E, 0x1E, 0xF};//, 0x2, 0x12, 0x3, 0x4, 0x14};
    uint64_t fails = 0;
    uint64_t runs = 0;
    /*for(auto mode : modes) {
        for(uint16_t a=0; a<=255; a++) {
            for(uint16_t b=0;b<=255; b++) {
                test_one(tb, a, b, mode, fails);
                runs +=1;
            }
        }
    }*/
    // Stabilize circuits so I may read into memory...
    printf("\tState:\t%x\n",tb->microalu_driver__DOT__state);
    tb->p1_in=0;tb->eval();
    //tb->microalu_driver__DOT__store__DOT__mem[0] = 0b00000000'00000000'0000'0'00;
    //tb->microalu_driver__DOT__store__DOT__mem[1] = 0b00001111'00000111'0011'0'00;
    //tb->microalu_driver__DOT__store__DOT__mem[2] = 0b00000000'00000000'0000'0'00;
    //tb->microalu_driver__DOT__store__DOT__mem[3] = 0b00000000'00000000'0000'0'00;
    //Begin stepping through microcode
    //tb->p1_in =0;tb->eval();tb->p1_in=1;tb->eval();
    for(int it=0; it <= 8; it ++) {
        tb->p1_in=0;tb->eval();tb->p1_in=3;tb->eval(); tb->p1_in=0;tb->eval();
        //uint32_t value = tb->microalu_driver__DOT__all;
        uint8_t a_value =  (tb->microalu_driver__DOT__a_mc) & 0xff;
        uint8_t b_value =  (tb->microalu_driver__DOT__b_mc) & 0xff;
        uint8_t alu_value =  (tb->microalu_driver__DOT__alu_func_mc) & 0xf;
        uint8_t cin_value =  (tb->microalu_driver__DOT__cin_mc) & 0b1;
        uint8_t display_value =  (0) & 0b11;
        
        printf("State:\t%x\n",tb->microalu_driver__DOT__state);
        //printf("\t\tMC:\t%08x\n", value);
        //assert(a_value == it);
        test_one(tb, a_value, b_value, alu_value | (cin_value << 4), fails);
    }
    /*std::cout << "Ran " << runs <<" unit tests." << std::endl;
    if(fails == 0) {
        std::cout << "Passed all unit tests." << std::endl;
    }
    else if(fails == 1) {
        std::cout << "Failed 1 unit test." << std::endl;
    }
    else{
        std::cout << "Failed " << fails <<" unit tests." << std::endl;
    }*/
    exit(EXIT_SUCCESS);
}
