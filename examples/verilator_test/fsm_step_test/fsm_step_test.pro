TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

TARGET = fsm_step_test

!defined(VERILATOR_PATH):exists(/usr/local/bin/verilator) {
    VERILATOR_PATH = /usr/local/bin/verilator
}
!defined(VERILATOR_PATH):exists(/usr/bin/verilator) {
    VERILATOR_PATH = /usr/bin/verilator
}
exists(/usr/local/share/verilator/include) {
    INCLUDEPATH += /usr/local/share/verilator/include
    VPATH += /usr/local/share/verilator/include
}
else:exists(/usr/share/verilator/include) {
    INCLUDEPATH += /usr/share/verilator/include
    VPATH += /usr/share/verilator/include
}

HEADERS += \
        ../../../../pep9milli/simmain/testbench.h \
        alufunc.h \
        cpu.h \
        defs.h \
        tests/fsm_gold.h



SOURCES += \
    tests/fsm_gold.cpp \
        tests/main.cpp \
        verilated.cpp \
        alufunc.c \
        cpu.c \

DISTFILES += \
        fsm_step_test.v \
        fsm_step_test.pcf \
        testbench.v \
        clock_gen.v \
        Makefile


INCLUDEPATH += $$OUT_PWD/obj_dir/

VPATH += $$PWD/../../../../pep9milli/simmain
INCLUDEPATH += $$PWD/../../../../pep9milli/simmain

# Generate verilator source code from verilog implementation.
verilator.target = $$OUT_PWD/obj_dir/Vfsm_driver.h
verilator.commands += $$VERILATOR_PATH --cc $$PWD/fsm_step_test.v --top-module fsm_driver  -Wno-LITENDIAN

unix{
    # Build verilator source code into a library
    make_verilate.target = $$OUT_PWD/obj_dir/libVfsm_driver__ALL.a
    make_verilate.commands += cd $$OUT_PWD/obj_dir/ && make -f Vfsm_driver.mk && mv Vfsm_driver__ALL.a libVfsm_driver__ALL.a
    make_verilate.depends = verilator
    # Add the ability to clean
    verilator_clean.commands = rm -rf $$OUT_PWD/obj_dir/
    clean.depends += verilator_clean
}
# Not yet implemented in Windows
win{
}

# Add verilog targets.
QMAKE_EXTRA_TARGETS += verilator make_verilate verilator_clean clean
# Force QMake to wait on Verilator output before building
unix{
    PRE_TARGETDEPS += $$OUT_PWD/obj_dir/Vfsm_driver.h $$OUT_PWD/obj_dir/libVfsm_driver__ALL.a
}
win{
}

# Link against verilator library.
unix|win32: LIBS += -L$$OUT_PWD/obj_dir/ -lVfsm_driver__ALL

# Make project aware of outputs from Verilator
INCLUDEPATH += $$OUT_PWD/obj_dir/
DEPENDPATH += $$OUT_PWD/obj_dir/
