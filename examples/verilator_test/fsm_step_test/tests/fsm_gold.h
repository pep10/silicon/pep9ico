#ifndef FSM_GOLD_H
#define FSM_GOLD_H

#include <stdint.h>
enum class fsm_states
{
    INITIAL = 0b00,
    COUNT   = 0b01,
    WAIT_BTN= 0b10,
    RESET   = 0b11,
};

struct state
{
    fsm_states fsm_current_state;
    uint8_t next, output;
    uint16_t enable_outputs;
    bool output_enable, overflow;
};

class fsm_gold
{
public:
    fsm_gold(uint8_t max_count);

    void reset();
    state step(bool enable_step, bool reset);
    const state get_current_state() const;
private:
    fsm_states transition(bool enable_step, bool reset);
    void update_state(uint8_t output, uint8_t next, bool output_enable, bool overflow);
    state current_state;
    const uint8_t max_count;

};

#endif // FSM_GOLD_H
