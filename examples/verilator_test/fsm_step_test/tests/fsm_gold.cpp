#include "fsm_gold.h"

fsm_gold::fsm_gold(uint8_t max_count): max_count(max_count)
{
    reset();
}

void fsm_gold::reset()
{
    // Reset state machine.
    current_state.fsm_current_state = fsm_states::INITIAL;

    // Reset counter outputs.
    current_state.output = 0;
    current_state.next = 0;
    current_state.overflow = 0;
    current_state.output_enable = 0;
}

state fsm_gold::step(bool enable_step, bool reset)
{
    fsm_states next = transition(enable_step, reset);
    /*switch (current_state.fsm_current_state) {
    case fsm_states::INITIAL:
        if(enable_step) {
            current_state.output = 0;
            current_state.next = 1;
            current_state.output_enable = 1;
            current_state.overflow = (max_count == current_state.output);
        }
        else {
            current_state.output = 0;
            current_state.next = 0;
            current_state.output_enable = 0;
            current_state.overflow = 0;
        }
        break;
    case fsm_states::COUNT:
            if(max_count -1 < current_state.next ) {
                current_state.overflow = 1;
                //current_state.output = current_state.next;
                current_state.output_enable = 1;
            }
            else {
                current_state.overflow = 0;
                current_state.output = current_state.next;
                current_state.next += 1;
                current_state.output_enable = 1;
            }

        break;
    case fsm_states::WAIT_BTN:
        [[fallthrough]];
    case fsm_states::RESET:
        current_state.output = 0;
        current_state.next = 0;
        current_state.output_enable = 0;
        current_state.overflow = 0;
    }*/
    current_state.fsm_current_state = next;

    return current_state;
}

const state fsm_gold::get_current_state() const
{
    return current_state;
}
void fsm_gold::update_state(uint8_t output, uint8_t next, bool output_enable, bool overflow)
{
    current_state.output = output;
    current_state.next = next;
    current_state.output_enable = output_enable;
    current_state.overflow = overflow;
    if(output_enable) {
        current_state.enable_outputs = 1 << (max_count - output - 1);
    }
    else {
        current_state.enable_outputs = 0;
    }
}
fsm_states fsm_gold::transition(bool enable_step, bool reset)
{
    if (reset) {
        update_state(0, 0, 0, 0);
        return fsm_states::INITIAL;
    }
    switch(current_state.fsm_current_state) {
    case fsm_states::INITIAL:
        if(enable_step) {
            update_state(0, 1, 1 ,0);
            return fsm_states::COUNT;
        }
        else {
            update_state(0, 0, 0 ,0);
            return fsm_states::INITIAL;
        }
    case fsm_states::COUNT:
        if(!current_state.overflow) {
            if(current_state.next < max_count) {
                update_state(current_state.next, current_state.next+1, 1,current_state.output+1>max_count);
            }
            else {
                update_state(current_state.output, current_state.next, 0, 1);
            }
            return fsm_states::COUNT;
        }
        else if(current_state.overflow &&  enable_step) {
            update_state(0, 0, 0, 0);
            return fsm_states::WAIT_BTN;
        }
        else {
            update_state(0, 0, 0, 0);
            return fsm_states::RESET;
        }
    case fsm_states::WAIT_BTN:
        update_state(0, 0, 0, 0);
        if(enable_step) return fsm_states::WAIT_BTN;
        else return fsm_states::RESET;
    case fsm_states::RESET:
        if(current_state.overflow) {
            update_state(0, 0, 0, 0);
            return fsm_states::RESET;
        }
        else if(enable_step) {
            update_state(0, 1, 1, 0);
            return fsm_states::COUNT;
        }
        else {
            update_state(0, 0, 0, 0);
            return fsm_states::INITIAL;
        }
    }
}
