#include <stdlib.h>
#include "Vfsm_driver.h"
#include "verilated.h"
#include "fsm_gold.h"

extern "C" {
#include "alufunc.h"
#include "defs.h"
#include "cpu.h"
}

#include <bitset>
#include <vector>
#include <iostream>
#include <random>

static int ticks=0;
void step(Vfsm_driver* driver)
{
    driver->p1_in = driver->p1_in & 0b1110;
    driver->eval();

    driver->p1_in = (driver->p1_in & 0b1110) | 0b0001;
    driver->eval();

    driver->p1_in = driver->p1_in & 0b1110;
    driver->eval();
    ticks++;
}
void reset(Vfsm_driver* driver)
{
    driver->p1_in &= 0b1101;
    driver->p1_in |= 0b0010;
    driver->eval();

    driver->p1_in &= 0b1101;
    driver->eval();
}
void input(Vfsm_driver* driver, bool step_enable, bool reset)
{
    driver->p1_in &= 0b1001;
    driver->p1_in |= step_enable ? 0b0100 : 0;
    driver->p1_in |= reset       ? 0b0010 : 0;
}
void print_state(Vfsm_driver* driver)
{
    std::cout << ticks;
    std::cout << "\t" << /*std::hex <<*/ std::bitset<4>(driver->p1_in) << std::endl;
    /*std::cout << "\t" << std::dec << std::bitset<2>(driver->fsm_driver__DOT__fsm_state)
              << " " << std::bitset<4>(driver->fsm_driver__DOT__clock_outputs)
              << " " << std::bitset<1>(driver->fsm_driver__DOT__machine__DOT__sequencer_output_enable)
              << " " << std::bitset<1>(driver->fsm_driver__DOT__machine__DOT__sequencer_enable)
              << " " << std::bitset<4>(driver->fsm_driver__DOT__machine__DOT__sequencer_state)
              << std::endl;*/
    std::cout << std::endl;
}

int main(int argc, char **argv) {
	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
    Vfsm_driver* driver = new Vfsm_driver("Top");
    fsm_gold model(8);
    bool x=true, y=false;

    std::cout << std::bitset<2>(driver->fsm_driver__DOT__fsm_state)
              << " "
              << std::bitset<2>(static_cast<uint8_t>(model.get_current_state().fsm_current_state))
              << std::endl;

    reset(driver);
    model.reset();
    std::default_random_engine generator;
    // 1/2 chance of the enable button being on
    std::uniform_int_distribution<bool> rand_enable(0, 1);
    // 1/50 chance of the reset being triggered.
    std::uniform_int_distribution<int>  rand_reset(0, 49);
    while(ticks < 0x100){
        input(driver, x, y);
        step(driver);
        auto r = model.step(x, y);
        std::cout << std::bitset<2>(driver->fsm_driver__DOT__fsm_state)
                  << " "
                  << std::bitset<4>(driver->fsm_driver__DOT__count)
                  << " "
                  << std::bitset<8>(driver->fsm_driver__DOT__enable_outputs)
                  << " "
                  << std::bitset<1>(r.output_enable)
                  << std::bitset<4>(r.output)
                  << " "
                  << std::bitset<2>(static_cast<uint8_t>(r.fsm_current_state))
                  << std::endl;
        assert(driver->fsm_driver__DOT__fsm_state ==
               static_cast<uint8_t>(r.fsm_current_state));
        assert(driver->fsm_driver__DOT__count == r.output);
        assert(driver->fsm_driver__DOT__enable_outputs == r.enable_outputs);
        //assert(driver->fsm_driver__DOT__seq_o_en == r.output_enable);
        //assert(driver->)
        x = rand_enable(generator);
        y = (rand_reset(generator) == 0);
    }
    exit(EXIT_SUCCESS);
}
