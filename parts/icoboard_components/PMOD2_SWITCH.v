module PMOD2_SWITCH(input [0:7] PMOD2_SWITCH_in, output[0:7] out);
    wire [0:7] bits_con;
    // Never directly connect inputs to outputs - buffer first.
    assign bits_con = PMOD2_SWITCH_in;
    buf bufo_0(out[0], bits_con[0]);
    buf bufo_1(out[1], bits_con[1]);
    buf bufo_2(out[2], bits_con[2]);
    buf bufo_3(out[3], bits_con[3]);
    buf bufo_4(out[4], bits_con[4]);
    buf bufo_5(out[5], bits_con[5]);
    buf bufo_6(out[6], bits_con[6]);
    buf bufo_7(out[7], bits_con[7]);
endmodule