module PMOD1_BUTTON(input [0:3] PMOD1_BUTTON_in, output[0:3] out);
    wire [0:3] bits_con;
    // Never directly connect inputs to outputs - buffer first.
    assign bits_con = PMOD1_BUTTON_in;
    buf bufo_0(out[0], bits_con[0]);
    buf bufo_1(out[1], bits_con[1]);
    buf bufo_2(out[2], bits_con[2]);
    buf bufo_3(out[3], bits_con[3]);
endmodule