module PMOD3_LED(input [0:7] in, output [0:7]PMOD3_LED_out);
    // Never directly connect inputs to outputs - buffer first.
    assign PMOD3_LED_out = in;
endmodule