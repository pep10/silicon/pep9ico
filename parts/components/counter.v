
// Synchronous N bit counter that wraps-around to 0 when reaching "maximum"
// Has a synchronous reset.
module counter #(parameter length=2, parameter maximum=(2**length)-1) (input clock, input reset, output reg [0:length-1] value);
    // Prevent device from being loaded with XX.
    initial begin 
        value = 0;
    end

    // Either force value to 0, or increment value.
    always @(posedge clock) begin
        if(reset || value >= maximum ) begin
            value <= 0;
        end
        else begin
            value <= value +1;
        end
    end
endmodule