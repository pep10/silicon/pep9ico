module demux1x16(output reg[0:15] y, input [0:3] select, input[0:0] d);
    //initial y = 16'h0000;
    always @(select or d) begin
        //$display("Display:%b||d:%b", select, d);
        // Blockingly assign a default value to all bits of y.
        // Then, blockingly assign the correct value to a bit.
        y = 16'h000;
        case (select)
            4'h0:  begin y[0] = d; end
            4'h1:  begin y[1] = d; end
            4'h2:  begin y[2] = d; end
            4'h3:  begin y[3] = d; end
            4'h4:  begin y[4] = d; end
            4'h5:  begin y[5] = d; end
            4'h6:  begin y[6] = d; end
            4'h7:  begin y[7] = d; end
            4'h8:  begin y[8] = d; end
            4'h9:  begin y[9] = d; end
            4'ha:  begin y[10] = d; end
            4'hb:  begin y[11] = d; end
            4'hc:  begin y[12] = d; end
            4'hd:  begin y[13] = d; end
            4'he:  begin y[14] = d; end
            4'hf:  begin y[15] = d; end
        endcase
        //$display("Y:%h", y);
    end 
endmodule