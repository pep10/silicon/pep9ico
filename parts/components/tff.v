module tff1(input Clk, input D, output reg Q);
    initial Q=1'b0;
    always @(posedge Clk) begin
        if(D) begin
            Q <= !Q;
        end
    end
endmodule