// Must include "selective_invert" for this module to work.

// Perform logical AND on 8-bit buses.
module and8(output [0:7] y, input [0:7] a, input [0:7] b);
    and a_7(y[7], a[7], b[7]);
    and a_6(y[6], a[6], b[6]);
    and a_5(y[5], a[5], b[5]);
    and a_4(y[4], a[4], b[4]);
    and a_3(y[3], a[3], b[3]);
    and a_2(y[2], a[2], b[2]);
    and a_1(y[1], a[1], b[1]);
    and a_0(y[0], a[0], b[0]);
endmodule

// Perform logical OR on 8-bit buses.
module or8(output [0:7] y, input [0:7] a, input [0:7] b);
    or o_7(y[7], a[7], b[7]);
    or o_6(y[6], a[6], b[6]);
    or o_5(y[5], a[5], b[5]);
    or o_4(y[4], a[4], b[4]);
    or o_3(y[3], a[3], b[3]);
    or o_2(y[2], a[2], b[2]);
    or o_1(y[1], a[1], b[0]);
    or o_0(y[0], a[0], b[1]);
endmodule

// Perform XOR on 8-bit buses.
module xor8(output [0:7] y, input [0:7] a, input [0:7] b);
    xor x_7(y[7], a[7], b[7]);
    xor x_6(y[6], a[6], b[6]);
    xor x_5(y[5], a[5], b[5]);
    xor x_4(y[4], a[4], b[4]);
    xor x_3(y[3], a[3], b[3]);
    xor x_2(y[2], a[2], b[2]);
    xor x_1(y[1], a[1], b[1]);
    xor x_0(y[0], a[0], b[0]);
endmodule

// Logical unit that computes AND/NAND/OR/NOR on 8-bit buses.
// This circuit is used in the Pep ALU because it uses fewer circuits to synthesize than separate AND/OR mechanisms.
// It (ab)uses De-Morgan's Law to compute all four functions in a single circuit.
// Listed below are functions computer for given values of invert_in, invert_out.
// in | out | function
// -------------------
//  0 |  0  | AND
//  0 |  1  | NAND
//  1 |  0  | NOR
//  1 |  1  | OR
module andor8(output [0:7] y, input [0:7] a, input [0:7] b, 
             input invert_in, input invert_out);
    wire [0:7] inverted_a, inverted_b, a_and_b;
    // Functional units to selectively invert A, B.
    selective_invert8 a_invert(inverted_a, invert_in, a);
    selective_invert8 b_invert(inverted_b, invert_in, b);
    // AND the (potentially inverted) inputs
    and8 and_ab(a_and_b, inverted_a, inverted_b);
    // Selectively invert output.
    selective_invert8 out_invert(y, invert_out, a_and_b);
endmodule