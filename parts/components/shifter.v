
module rshift8(output [0:7] Y, output cout, input [0:7] A, input cin);

    assign Y[1:7] = A[0:6];
    assign cout = A[7];
    assign Y[0] = cin;
    
endmodule

module lshift8(output [0:7] Y, output cout, output vout, input [0:7] A, input cin);

    assign Y[0:6] = A[1:7];
    assign cout = A[0];
    assign Y[7] = cin;
    
    // Signed overflow if A[0], A[1] do not match.
    xor signed_overflow(vout, A[0], A[1]);
    
endmodule