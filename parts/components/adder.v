// Enable gates are AND gates with a data input and a control
module adder1(output[0:0] out_data, output cout, input [0:0] a, input[0:0] b, input invert_b, input cin);
    wire b_inverted, ab_xored;
    wire first_carry, second_carry; 

    // Compute A+B
    xor xor_b(b_inverted, invert_b, b[0]);
    xor xor_ab(ab_xored, a[0], b_inverted);
    xor xor_abcin(out_data[0], cin, ab_xored);

    // Computer carry out.
    and carry1(first_carry, a[0], b_inverted);
    and carry2(second_carry, cin, ab_xored);
    or  carryor(cout, first_carry, second_carry);

endmodule
module adder1v(output[0:0] out_data, output cout, output vout, input [0:0] a, input[0:0] b, input invert_b, input cin);
    
    // Re-use adder with no V bit.
    adder1 add(out_data, cout, a, b, invert_b, cin);

    // Compute V bit.
    wire b_inv, ab_xored, neg_abcomp;
    wire aout_xored;

    // Set ab_xored to 1 if A, B have the same sign.
    // Must selectively invert B.
    xor invb(b_inv, b, invert_b);
    xor xor1(ab_xored, a, b_inv);
    not not1(neg_abcomp, ab_xored);
    // Set aout_xored to 1 if a, out_data differ in sign.
    xor xor2(aout_xored, a, out_data);
    // Signed overflow only occurs on both above conditions.
    and and1(vout, neg_abcomp, aout_xored);

endmodule

// Enable gates are AND gates with a data input and a control
module adder4v(output[0:3] out_data, output cout, output vout, input [0:3] a, input[0:3] b, input invert_b, input cin);
    wire chain_carry[0:2];

    // Implement adder as a 4 bit ripple carry adder
    adder1  bit3(out_data[3], chain_carry[2], a[3], b[3], invert_b, cin);
    adder1  bit2(out_data[2], chain_carry[1], a[2], b[2], invert_b, chain_carry[2]);
    adder1  bit1(out_data[1], chain_carry[0], a[1], b[1], invert_b, chain_carry[1]);
    adder1v bit0(out_data[0], cout, vout,     a[0], b[0], invert_b, chain_carry[0]);
    
endmodule

// Enable gates are AND gates with a data input and a control
module adder8v(output[0:7] out_data, output cout, output vout, input [0:7] a, input[0:7] b, input invert_b, input cin);
    wire chain_carry[0:6];

    // Implement adder as a 8 bit ripple carry adder
    adder1  bit7(out_data[7], chain_carry[6], a[7], b[7], invert_b, cin);
    adder1  bit6(out_data[6], chain_carry[5], a[6], b[6], invert_b, chain_carry[6]);
    adder1  bit5(out_data[5], chain_carry[4], a[5], b[5], invert_b, chain_carry[5]);
    adder1  bit4(out_data[4], chain_carry[3], a[4], b[4], invert_b, chain_carry[4]);
    adder1  bit3(out_data[3], chain_carry[2], a[3], b[3], invert_b, chain_carry[3]);
    adder1  bit2(out_data[2], chain_carry[1], a[2], b[2], invert_b, chain_carry[2]);
    adder1  bit1(out_data[1], chain_carry[0], a[1], b[1], invert_b, chain_carry[1]);
    adder1v bit0(out_data[0], cout, vout,     a[0], b[0], invert_b, chain_carry[0]);
    
endmodule

// Enable gates are AND gates with a data input and a control
module adder #(parameter length=8)(output reg [0:length-1] out_data, output reg cout, output reg vout, input [0:length-1] a, input[0:length-1] b, input invert_b, input cin);

    reg [0:length-1] temp_b;
    always @* begin
        temp_b = b ^ {length{invert_b}};
        /* verilator lint_off WIDTH */
        {cout, out_data} = {1'b0,a} + {1'b0,temp_b} + cin;
        /* verilator lint_on WIDTH */
        // There is signed overflow is A/B match in sign and A/Out differ in sign.
        vout = (a[0] == temp_b[0]) && (a[0] != out_data[0]);
    end
endmodule