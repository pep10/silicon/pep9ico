module mem512x8(input clock, input write_enable, input read_enable, input [0:8] addr, input [0:7] wdata, output reg [0:7] rdata);
  reg [0:7] mem [0:511];
  initial begin
  mem[0] = 8'hff;
  mem[1] = 8'hee;
  mem[2] = 8'hdd;
  mem[3] = 8'hcc;
  mem[4] = 8'hbb;
  mem[5] = 8'haa;
  mem[6] = 8'h99;
  mem[7] = 8'h88;
  end
  always @(posedge clock) begin
        if (write_enable) mem[addr] <= wdata;
        if(read_enable) rdata <= mem[addr];
        else rdata <= 8'h00;
  end
endmodule
