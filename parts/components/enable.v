// Enable gates are AND gates with a data input and a control
module enable1(input enable, input[0:0] in_data, output[0:0] out_data);
    wire [0:0] data_wire;
    and a_0(data_wire[0], in_data[0], enable);

    assign out_data = data_wire;
endmodule

// Enable gate for a four bit data bus.
module enable3(input enable, input[0:2] in_data, output[0:2] out_data);
    wire [0:2] data_wire;

    and a_2(data_wire[2], in_data[2], enable);
    and a_1(data_wire[1], in_data[1], enable);
    and a_0(data_wire[0], in_data[0], enable);

    assign out_data = data_wire;
endmodule

// Enable gate for a four bit data bus.
module enable4(input enable, input[0:3] in_data, output[0:3] out_data);
    wire [0:3] data_wire;

    and a_3(data_wire[3], in_data[3], enable);
    and a_2(data_wire[2], in_data[2], enable);
    and a_1(data_wire[1], in_data[1], enable);
    and a_0(data_wire[0], in_data[0], enable);

    assign out_data = data_wire;
endmodule

// Enable gate for a eight bit data bus.
module enable8(input enable, input[0:7] in_data, output[0:7] out_data);
    wire [0:7] data_wire;
    and a_7(data_wire[7], in_data[7], enable);
    and a_6(data_wire[6], in_data[6], enable);
    and a_5(data_wire[5], in_data[5], enable);
    and a_4(data_wire[4], in_data[4], enable);
    and a_3(data_wire[3], in_data[3], enable);
    and a_2(data_wire[2], in_data[2], enable);
    and a_1(data_wire[1], in_data[1], enable);
    and a_0(data_wire[0], in_data[0], enable);

    assign out_data = data_wire;
endmodule