// Selective invert_controler for on
module selective_invert1(output[0:0] out_data, input invert_control, input[0:0] in_data);
    xor x_0(out_data[0], in_data[0], invert_control);
endmodule

// Selective invert_controler for a four bit data bus.
module selective_invert4(output[0:3] out_data, input invert_control, input[0:3] in_data);
    xor x_3(out_data[3], in_data[3], invert_control);
    xor x_2(out_data[2], in_data[2], invert_control);
    xor x_1(out_data[1], in_data[1], invert_control);
    xor x_0(out_data[0], in_data[0], invert_control);
endmodule

// Selective invert_controler for a eight bit data bus.
module selective_invert8(output[0:7] out_data, input invert_control, input[0:7] in_data);
    xor x_7(out_data[7], in_data[7], invert_control);
    xor x_6(out_data[6], in_data[6], invert_control);
    xor x_5(out_data[5], in_data[5], invert_control);
    xor x_4(out_data[4], in_data[4], invert_control);
    xor x_3(out_data[3], in_data[3], invert_control);
    xor x_2(out_data[2], in_data[2], invert_control);
    xor x_1(out_data[1], in_data[1], invert_control);
    xor x_0(out_data[0], in_data[0], invert_control);
endmodule