`default_nettype none

// 1 bit D-FF with no set/reset.
module dff1(Clk, D, Q);
  input Clk;
  input D;
  output Q;

  /* user-specified Verilog code */
  //---- SB_DFF
  /**/    reg Q = 0;
  /**/    always @(posedge Clk) begin
  /**/        Q <= D;
  /**/    end

endmodule   /* pepcomp__1DFF */

// 8 bit D-FF with no set/reset.
module dff8(Clk, D, Q);
  input Clk;
  input [0:7] D;
  output [0:7] Q;

  dff1 bit_0(.Clk(Clk), .D(D[0]), .Q(Q[0]));
  dff1 bit_1(.Clk(Clk), .D(D[1]), .Q(Q[1]));
  dff1 bit_2(.Clk(Clk), .D(D[2]), .Q(Q[2]));
  dff1 bit_3(.Clk(Clk), .D(D[3]), .Q(Q[3]));
  dff1 bit_4(.Clk(Clk), .D(D[4]), .Q(Q[4]));
  dff1 bit_5(.Clk(Clk), .D(D[5]), .Q(Q[5]));
  dff1 bit_6(.Clk(Clk), .D(D[6]), .Q(Q[6]));
  dff1 bit_7(.Clk(Clk), .D(D[7]), .Q(Q[7]));
endmodule   /* pepcomp__8DFF */

// 1 bit D-FF with no set/reset.
module dffn1(Clk, D, Q);
  input wire Clk;
  input wire D;
  output reg Q;
  initial begin
    Q = 0;
  end
  /* user-specified Verilog code */
  //---- SB_DFF
  /**/    always @(negedge Clk) begin
  /**/        Q <= D;
  /**/    end

endmodule   /* pepcomp__1DFF */

// 8 bit falling edge D-FF with no set/reset.
module dffn8(Clk, D, Q);
  input Clk;
  input [0:7] D;
  output [0:7] Q;

  dffn1 bit_0(.Clk(Clk), .D(D[0]), .Q(Q[0]));
  dffn1 bit_1(.Clk(Clk), .D(D[1]), .Q(Q[1]));
  dffn1 bit_2(.Clk(Clk), .D(D[2]), .Q(Q[2]));
  dffn1 bit_3(.Clk(Clk), .D(D[3]), .Q(Q[3]));
  dffn1 bit_4(.Clk(Clk), .D(D[4]), .Q(Q[4]));
  dffn1 bit_5(.Clk(Clk), .D(D[5]), .Q(Q[5]));
  dffn1 bit_6(.Clk(Clk), .D(D[6]), .Q(Q[6]));
  dffn1 bit_7(.Clk(Clk), .D(D[7]), .Q(Q[7]));
endmodule   /* pepcomp__8DFF */