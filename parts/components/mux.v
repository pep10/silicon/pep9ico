module mux2xn #(parameter length=8)(output reg [0:length-1] y, input [0:0] select, input [0:length-1] d0, input [0:length-1] d1);
    always @(select or d0 or d1) begin
        //$display("Display:%b", select);
        case (select)
            1'b0:  y = d0;
            1'b1:  y = d1;
        endcase
        //$display("Y:%h", y);
    end 
endmodule

module mux4x8(output reg [0:7] y, input [0:1] select, 
    input [0:7] d0, input [0:7] d1, input [0:7] d2, input [0:7] d3);
    always @(select or d0 or d1 or d2 or d3) begin
        case (select)
            2'b00:  y = d0;
            2'b01:  y = d1;
            2'b10:  y = d2;
            2'b11:  y = d3;
        endcase
    end 
endmodule

module mux8x8(output reg [0:7] y, input [0:2] select, 
    input [0:7] d0, input [0:7] d1, input [0:7] d2, input [0:7] d3,
    input [0:7] d4, input [0:7] d5, input [0:7] d6, input [0:7] d7);
    always @(select or d0 or d1 or d2 or d3 or d4 or d5 or d6 or d7) begin
        case (select)
            3'b000:  y = d0;
            3'b001:  y = d1;
            3'b010:  y = d2;
            3'b011:  y = d3;
            3'b100:  y = d4;
            3'b101:  y = d5;
            3'b110:  y = d6;
            3'b111:  y = d7;
        endcase
    end 
endmodule

module muxnx8 #(parameter length=8) (output reg [0:length-1] y, input [0:2] select, 
    input [0:length-1] d0, input [0:length-1] d1, input [0:length-1] d2, input [0:length-1] d3,
    input [0:length-1] d4, input [0:length-1] d5, input [0:length-1] d6, input [0:length-1] d7);
    always @(select or d0 or d1) begin
        case (select)
            3'b000:  y = d0;
            3'b001:  y = d1;
            3'b010:  y = d2;
            3'b011:  y = d3;
            3'b100:  y = d4;
            3'b101:  y = d5;
            3'b110:  y = d6;
            3'b111:  y = d7;
        endcase
    end 
endmodule