
// Must include mux.v, demux.v
module PEP9_ALU(output [0:7] Y, output NWire, output Zwire, output V, output C,
                input [0:7] A, input [0:7] B, input [0:3] ALUFuncSelect, input Cin);
    // Decode ALU function selection.
    wire [0:15] ALUFunction;
    demux1x16 ALUFuncDecoder(ALUFunction, ALUFuncSelect, 1'b1);



    // ADDER / SUBTRACTER functional unit.
    wire [0:7] ADD_out;
    wire [0:1] ADD_VC_out;
    wire ADD_enable;

    wire ADD_invert_b, ADD_cin, ADD_T1, ADD_T2;
    // Compute the value of the carry in.
    // If ALUFunc=3, then the carry must be set to 1.
    // If the ALUFunc={1,3}, then the external cin must be ignored.
    // If ALUFunc={2,4}, the external cin should be routed through the adders.
    nor  ADD_nocarry(ADD_T1, ALUFunction[1], ALUFunction[3]);
    and (ADD_T2, Cin, ADD_T1);
    or  ADD_cin_compute(ADD_cin, ADD_T2, ALUFunction[3]);
    // Set the invert line if ALUFunc={3,4}.
    or ADD_invert_b_gate(ADD_invert_b, ALUFunction[3], ALUFunction[4]);
    // With all flags set, compute the addition.
    adder #(8) ADD_unit(ADD_out, ADD_VC_out[1], ADD_VC_out[0], A, B, ADD_invert_b, ADD_cin );
    or ADD_enable_gate(ADD_enable, ALUFunction[1], ALUFunction[2], ALUFunction[3], ALUFunction[4]);



    // Logical (AND / OR / NAND / NOR) functional unit
    wire [0:7] LOG_out;
    wire [0:1] LOG_VC_out = 2'b00;
    wire LOG_enable;

    wire LOG_invert_in, LOG_invert_out;
    // If ALUFunc={7,8}, invert inputs.
    or LOG_or1(LOG_invert_in,  ALUFunction[7], ALUFunction[8]);
    // If ALUFunc={6,7}, invert inputs.
    or LOG_or2(LOG_invert_out, ALUFunction[6], ALUFunction[7]);
    // Compute AND/NAND/OR/NOR of A B.
    andor8 LOG_unit(LOG_out, A, B, LOG_invert_in, LOG_invert_out);
    or LOG_enable_gate(LOG_enable, ALUFunction[5], ALUFunction[6], ALUFunction[7], ALUFunction[8]);



    // IDENTITY / INVERT functional unit.
    wire [0:7] IDN_out;
    wire [0:1] IDN_VC_out = 2'b00;
    wire IDN_enable;

    // Invert output if function is 10, otherwise pass A through unmodified
    selective_invert8 IDN_unit(IDN_out, ALUFunction[10], A);
    or IDN_enable_gate(IDN_enable, ALUFunction[0], ALUFunction[10]);



    // XOR functional unit.
    wire [0:7] XOR_out;
    wire [0:1] XOR_VC_out = 2'b00;
    wire XOR_enable;

    // Perform XOR when ALUFunction=9.
    xor8 XOR_unit(XOR_out, A, B);
    assign XOR_enable = ALUFunction[9];



    // LEFT SHIFT functional unit.
    wire [0:7] LSF_out;
    wire [0:1] LSF_VC_out;
    wire LSF_enable;
    
    wire LSF_cin;
    // Carry in IFF using ROL (12).
    and LSF_12(LSF_cin, ALUFunction[12], Cin);
    lshift8 LSF_unit(LSF_out, LSF_VC_out[1], LSF_VC_out[0], A, LSF_cin);
    or LSF_enable_gate(LSF_enable, ALUFunction[11], ALUFunction[12]);



    // RIGHT SHIFT functional unit.
    wire [0:7] RSF_out;
    wire [0:1] RSF_VC_out;
    assign RSF_VC_out[0] = 0;
    wire RSF_enable;
    
    wire RSF_cin, RSF_T1, RSF_T2;
    // If rotate (14), use carry in bit.
    and RSF_14(RSF_T1, ALUFunction[14], Cin);
    // If shift (13), sign extend into carry.
    and RSF_13(RSF_T2, ALUFunction[13], A[0]);
    // Choose either carry in.
    or RSF_or1(RSF_cin, RSF_T2, RSF_T1);
    rshift8 RSF_unit(RSF_out, RSF_VC_out[1], A, RSF_cin);
    or RSF_enable_gate(RSF_enable, ALUFunction[13], ALUFunction[14]);

    // STATUS bits controls
    wire [0:7] STS_out = 8'h00;
    wire [0:1] STS_VC_out = A[6:7];
    wire STS_enable;
    assign STS_enable = ALUFunction[15];

    
    // Select which output should be routed through the MUX.
    wire [0:2] mux_select_out;
    wire [0:2] mux_select0,mux_select1,mux_select2,mux_select3,mux_select4,mux_select5,mux_select6;
    enable3 ADD_gate(ADD_enable, 3'b000, mux_select0);
    enable3 LOG_gate(LOG_enable, 3'b001, mux_select1);
    enable3 IDN_gate(IDN_enable, 3'b010, mux_select2);
    enable3 XOR_gate(XOR_enable, 3'b011, mux_select3);
    enable3 LSF_gate(LSF_enable, 3'b100, mux_select4);
    enable3 RSF_gate(RSF_enable, 3'b101, mux_select5);
    enable3 STS_gate(STS_enable, 3'b110, mux_select6);
    // Multiplex values.
    //or(mux_select_out[0], mux_select[0][0], mux_select[1][0], mux_select[2][0], mux_select[3][0], mux_select[4][0], mux_select[5][0], mux_select[6][0]);
    //or(mux_select_out[1], mux_select[0][1], mux_select[1][1], mux_select[2][1], mux_select[3][1], mux_select[4][1], mux_select[5][1], mux_select[6][1]);
    //or(mux_select_out[2], mux_select[0][2], mux_select[1][2], mux_select[2][2], mux_select[3][2], mux_select[4][2], mux_select[5][2], mux_select[6][2]);
    or(mux_select_out[0], mux_select0[0], mux_select1[0], mux_select2[0], mux_select3[0], mux_select4[0], mux_select5[0], mux_select6[0]);
    or(mux_select_out[1], mux_select0[1], mux_select1[1], mux_select2[1], mux_select3[1], mux_select4[1], mux_select5[1], mux_select6[1]);
    or(mux_select_out[2], mux_select0[2], mux_select1[2], mux_select2[2], mux_select3[2], mux_select4[2], mux_select5[2], mux_select6[2]);
    // Output the correct data.
    mux8x8 data_mux(Y, mux_select_out, 
        ADD_out, LOG_out, IDN_out, XOR_out,
        LSF_out, RSF_out, STS_out, 8'h00);
    wire all_zero;
    or(all_zero, Y[0], Y[1], Y[2], Y[3], Y[4], Y[5], Y[6], Y[7]);
    muxnx8 #(1) VMux (V, mux_select_out, ADD_VC_out[0], LOG_VC_out[0], IDN_VC_out[0], XOR_VC_out[0], LSF_VC_out[0], RSF_VC_out[0], STS_VC_out[0], 1'b0);
    muxnx8 #(1) CMux (C, mux_select_out, ADD_VC_out[1], LOG_VC_out[1], IDN_VC_out[1], XOR_VC_out[1], LSF_VC_out[1], RSF_VC_out[1], STS_VC_out[1], 1'b0);
    mux2xn #(1) NMux (NWire, STS_enable, Y[0], A[4]);
    mux2xn #(1) ZMux (Zwire, STS_enable, ~all_zero ,A[5]);



endmodule