module ClockSequenceGenerator #(parameter clock_count=8)
(input wire global_clock, input wire reset, input wire enable_step,
 output reg [0:clock_count-1] clock_outputs,
 output [0:1] fsm_state,
 output [0:$clog2(clock_count)] count);
	wire sequencer_overflow;
	wire sequencer_reset, sequencer_enable;
	SequenceFSM state_machine
	    (.clock(global_clock),
		 .reset(reset),
		 .step_enable(enable_step),
		 .step_finished(sequencer_overflow),
		 .clock_reset(sequencer_reset),
		 .clock_enable(sequencer_enable),
		 .state_current(fsm_state));
	wire sequencer_output_enable;
	ClockSequencer #(.max_count(clock_count)) sequencer
	                (.clock(global_clock),
					 .reset(sequencer_reset|reset),
					 .count_enable(sequencer_enable),
					 .clock_enable(sequencer_output_enable),
					 .count(count),
					 .overflow(sequencer_overflow));
	reg [0:$clog2(clock_count)] i;
	always @ (*) begin
	        for (i=0; i<clock_count; i=i+1) begin // Instantiate L multipliersassign
			    if(sequencer_output_enable & (count == i)) begin
				clock_outputs[i] = 1;
			end
			else clock_outputs[i] = 0;
		end
	end
endmodule

// Finite state machine to generate a single "step" through all applicable clocks
module SequenceFSM (input clock, input reset,
                    input wire step_enable, input wire step_finished,
					output reg clock_reset, output reg clock_enable,
					output reg [0:1] state_current);
	localparam STATE_INITIAL = 2'b00,
	STATE_COUNT = 2'b01,
	STATE_WAIT_EN = 2'b10,
	STATE_RESET = 2'b11;

    reg [0:1] state_next;
	// Set up initial values to reasonable defaults
	initial begin
		state_current = STATE_INITIAL;
		state_next = STATE_INITIAL;
		clock_reset = 0;
		clock_enable = 0;
	end

	// Transition to the next state in the system.
	always @(posedge clock) begin
		if(reset) state_current <= STATE_INITIAL;
		else state_current <= state_next;
	end

	// Define transition logic according to diagram.
	always @(*) begin
		case(state_current)
			STATE_INITIAL: begin
				if(step_enable) state_next = STATE_COUNT;
				else state_next = STATE_INITIAL;
			end
			STATE_COUNT: begin
				if(!step_finished) state_next = STATE_COUNT;
				else if(step_finished & step_enable) state_next = STATE_WAIT_EN;
				else if(step_finished & !step_enable) state_next = STATE_RESET;
			end
			STATE_WAIT_EN: begin
				if(step_enable) state_next = STATE_WAIT_EN;
				else state_next = STATE_RESET;
			end
			STATE_RESET: begin
			    if(step_finished) state_next = STATE_RESET;
				else if(step_enable) state_next = STATE_COUNT;
				else state_next = STATE_INITIAL;
			end
		endcase
	end

	// Define outputs based on current state
	// Note that the outputs are based on the current state, rather than the transitions.
	always @(*) begin
	    case (state_next)
		    STATE_INITIAL: begin
			    clock_reset  = 0;
				clock_enable = 0;
			end
			STATE_COUNT: begin
			    clock_reset  = 0;
				clock_enable = 1;
			end
			STATE_WAIT_EN: begin
			    clock_reset  = 1;
				clock_enable = 0;
				end
			STATE_RESET: begin
			    clock_reset  = 1;
				clock_enable = 0;
			end
		endcase
	end

endmodule

// Count from [0, max_count], and output clock_enable if the output should be used.
// The count is stored in count, and overflow indicates that the counter has exceeded its maximum size.
// The counter will not wrap around once overflowed. It must be reset via the reset input.
// count_enable allows you to selectively turn the counter on/off. 
module ClockSequencer #(parameter max_count=9)(input clock, input reset, input count_enable, output reg clock_enable,
                                               output reg [0:($clog2(max_count))] count, output reg overflow);

    reg [0:($clog2(max_count))] next_count;
	initial begin
		clock_enable = 0;
		count = 0;
		next_count = 0;
		overflow = 0;
	end

	// Increment on the NEXT clock cycle rather than this one.
	// This allows the number "0" to become a valid state
	always @(posedge clock) begin
	    overflow = 0;
		clock_enable = 0;
		if(reset) begin
		    count = 0;
			next_count = 0;
		end
		else if (count_enable) begin
		    if(count == max_count-1) begin
			    overflow = 1;
			end
			else begin
			    count = next_count;
				next_count = next_count + 1;
				clock_enable = 1;
			end
		end
	end
endmodule
