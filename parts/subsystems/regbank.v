module RegisterBank(input [0:4] ASelect, input [0:4] BSelect, input [0:4] CSelect, input [0:7] CData,
					input AEnable, input BEnable, input CEnable,
					input ACkEnable, input BCkEnable, WCkEnable, input clock, input reset,
					output reg [0:7] AData=0, output reg [0:7] BData=0
					);
	wire [0:7] reg_out;
	reg [0:7] reg_address;
	//dffn8 ALatch(ACkEnable&clock, reg_out, AData);
	//dffn8 BLatch(BCkEnable&clock, reg_out, BData);
	mem512x8 registers(.clock(clock),
					   .write_enable(CEnable),
					   .read_enable(1'b1),
					   .addr((AEnable? ASelect:(BEnable?BSelect:9'h0))),
					   .wdata(CData),
					   .rdata(reg_out));
	initial begin
	end
	always @(posedge clock) begin
		//$display("I have an address %b\t%b", ASelect, reg_out);
		if(AEnable) reg_address = {3'h0, ASelect};
		else if(BEnable) reg_address = {3'h0, BSelect};
	end
	always @(negedge clock) begin
		if(reset) begin
			AData = 8'h0;
			BData = 8'h0;
		end
		else if(ACkEnable) begin
			AData = reg_out;
		end
		else if(BCkEnable) begin
			BData = reg_out;
		end

	end
	/*always @(posedge clock) begin
		if(reset) begin
			
		end
		else begin
			if(AEnable) begin
				reg_address = {3'h0, ASelect};
			end
			else if(BEnable) begin
				reg_address = {3'h0, BSelect};
			end
			else if(CEnable) begin
				reg_address = {3'h0, CSelect};
			end
		end

	end*/
endmodule