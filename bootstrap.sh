sudo apt-get install build-essential clang bison flex libreadline-dev \
                     gawk tcl-dev libffi-dev git mercurial graphviz   \
                     xdot pkg-config python python3 libftdi-dev \
                     qt5-default python3-dev libboost-all-dev cmake

# Needed to visualize waveforms of verilog programs.
sudo apt install gtkwave
 
# Needed to connect to a TTY instance for GPIO pins.
sudo apt install screen

# Install icarus verilog
git clone git://github.com/steveicarus/iverilog.git iverilog
sudo apt-get install autoconf bison flex gperf libreadline6 \
        libreadline6-dev libncurses5-dev
(
        cd iverilog
        bash autoconf.sh
        ./configure
        make -j$(nproc)
        sudo make install
)

# Install yosys synthesis tool
git clone https://github.com/cliffordwolf/yosys.git yosys
(
	cd yosys
	make -j$(nproc)
	sudo make install
)

# Install tool that communicates with GPIO pins
https://github.com/WiringPi/WiringPi
(
	cd WiringPi
	./build
)

# Create icestorm, which contains most tools needed by open-source framework.
git clone https://github.com/cliffordwolf/icestorm.git icestorm
(
	cd icestorm
	make -j$(nproc)
	sudo make install
)

# Install Arachne, outdated but used in class
git clone https://github.com/cseed/arachne-pnr.git arachne-pnr
(
	cd arachne-pnr
	make -j$(nproc)
	sudo make install
)

# Build NextPNR, must disable GUI or crash.
git clone https://github.com/YosysHQ/nextpnr nextpnr
(
	cd nextpnr
	cmake -DARCH=ice40 -DBUILD_GUI=OFF .
	make -j$(nproc)
	sudo make install
)

# Build ICO tools, which depends on WiringPi.
git clone https://github.com/cliffordwolf/icotools
(
	cd icotools/icoprog
	make
	sudo make install
)
